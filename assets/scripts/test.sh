#!/bin/sh
# test.sh -- make sure the demo site is minimally functional
# Copyright (C) 2016, 2018  Olaf Meeuwissen
#
# License: GPL-3.0+

PORT=8000

# Initialize whe website
nikola init --quiet --demo test
sed -i '/COMMENT_SYSTEM/s/".*/""/' test/conf.py
cd test && nikola build && nikola serve -d -p ${PORT} && cd -

BASE="http://localhost:${PORT}"

python ~/site/test.py "${BASE}"
