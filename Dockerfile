#  Nikola run-time environment
#  Copyright (C) 2023 Victor Santos
#
#  License: GPL-3.0+

FROM python:3.8-slim
MAINTAINER Victor Santos <vct.santos@protonmail.com>
ARG NIKOLA_VERSION

ARG NIKOLA_USER=tesla

ENV \
    DART_SASS_VERSION=1.60.0 \
    ############################################
    # Disable pip caching and version checking #
    ############################################
    PIP_NO_INPUT=1 \
    PIP_NO_CACHE_DIR=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=1 \
    ######################################
    # Ignore action when running as root #
    ######################################
    PIP_ROOT_USER_ACTION=ignore \
    #############################################################
    # Avoid to write .pyc files on the import of source modules #
    #############################################################
    PYTHONDONTWRITEBYTECODE=1 \
    ####################################################
    # Ensures that the python output i.e. the stdout   #
    # and stderr streams are sent straight to terminal #
    ####################################################
    PYTHONUNBUFFERED=1    

RUN \
    DEBIAN_FRONTEND=noninteractive \
    apt-get update --assume-yes && \
    ########################
    # Install dependencies #
    ########################
    apt-get install --assume-yes \
            curl \
            unzip \
            && \
    #########################
    # Install SASS compiler #
    #########################
    curl --remote-name --location https://github.com/sass/dart-sass/releases/download/${DART_SASS_VERSION}/dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz && \
    tar zxvf dart-sass-${DART_SASS_VERSION}-linux-x64.tar.gz && \
    mv dart-sass/sass /usr/bin/sass && \
    rm -rf dart-sass && \
    #############
    # Clean all #
    #############
    apt-get autoremove --assume-yes && \
    apt-get autoclean && \
    rm --force --recursive /var/lib/apt/lists/* /var/log/dpkg.log && \
    ##############
    # Update pip #
    ##############
    pip install --upgrade pip && \
    #######################
    # Create regular user #
    #######################
    useradd --create-home --shell /bin/bash ${NIKOLA_USER}

USER ${NIKOLA_USER}
WORKDIR /home/${NIKOLA_USER}
ENV PATH="/home/${NIKOLA_USER}/.local/bin:${PATH}"
COPY --chown=${NIKOLA_USER}:${NIKOLA_USER} assets/requirements.txt /tmp/requirements.txt
COPY --chown=${NIKOLA_USER}:${NIKOLA_USER} assets/constraints.txt /tmp/constraints.txt
RUN \
    pip install pip setuptools wheel && \
    pip install --requirement /tmp/requirements.txt --constraint /tmp/constraints.txt && \
    rm -rf /tmp/*.txt && rm -Rf /home/${NIKOLA_USER}/.cache/

CMD ["nikola", "--version"]
