# Docker Image for Nikola

This is a [Docker](https://docs.docker.com/) image for the
[Nikola](https://getnikola.com/) static site generator. It is based on [Python
official image](https://hub.docker.com/_/python) and uses the latest version of
Nikola, combined with the [CommonMark](https://commonmark.org/) package for
using the [extra Nikola plugin](https://plugins.getnikola.com/v8/commonmark/).
All Nikola "extras" are available. Additional requirements for any "extras" that
you use need to be installed manually. This can be done by deriving a custom
Docker image `FROM` this one. All languages supported by Nikola can be used.

You can pull the most recent pre-built image directly from the GitLab Container
Registry with:

```
docker pull registry.gitlab.com/padawanphysicist/nikola
```

This image sets `/bin/sh` as its default command.


## Using the Image

Assuming your Nikola site lives in the current directory, you can use something
like

```
docker run --rm -it -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       registry.gitlab.com/padawanphysicist/nikola /bin/bash
```

to start a container. Your site will be volume mounted on `/site`, which is also
the working directory inside the container. The `-u` option will cause all files
created by `nikola` to be owned by the user and group IDs that you have
*outside* the container. This makes life easier if you want to modify or remove
files after you have left the container.

Once in the container, you can use the regular `nikola` commands to maintain
your site interactively. Of course, you can also start up a container for a
one-off `nikola` command. For example

```
docker run --rm -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       registry.gitlab.com/padawanphysicist/nikola nikola check -l
```

would check your site for dangling links.


## Creating a Nikola Site

If you don't have a site yet you can run `nikola init` in an interactive
container. You can also run a one-off, like so

```
docker run --rm -it -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       registry.gitlab.com/padawanphysicist/nikola nikola init .
```

If you prefer to skip all the questions, pass the `--quiet` option to the
`nikola init` command and edit the `conf.py` manually once the command has
finished.


## Serving Your Site Locally

You can also proofread your site, using `nikola`'s `serve` command. It serves
your site on port 8000 by default but you have to expose that container port in
order to make it accessible from the outside. That goes like this (note the `-p`
option):

```
docker run --rm -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       -p 8888:8000 registry.gitlab.com/padawanphysicist/nikola nikola serve
```

Now you can browse your site at `http://localhost:8888/`.

Even better is to make `nikola` monitor the source for your site for changes and
update it automatically:

```
docker run --rm -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       -p 8888:8000 registry.gitlab.com/padawanphysicist/nikola nikola auto \
       --address 0.0.0.0
```

The `--address` option is needed to make the site accessible from outside the
container, something that the `serve` command does by default.

Use `Ctrl+C` to shut down the container process.


## Alias for running the container

You can avoid type most of the commands above by defining a shell `alias` by
observing that the initial part of most commands is the same. The `-p` is not
needed by some of the commands but will not do any harm, so

```
alias nikola='docker run --rm -v $PWD:/site -w /site \
          -u $(id -u):$(id -g) -p 8888:8000 \
          registry.gitlab.com/padawanphysicist/nikola nikola'
```
will let you get away with short commands such as `nikola help` as
if `nikola` was directly installed on your system. You will not be
able to enter the container with `nikola /bin/sh` though.
