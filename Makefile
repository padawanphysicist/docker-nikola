NIKOLA_VERSION=8.2.3
NIKOLA_USER=tesla

.PHONY: test

all: build test version

build: Dockerfile
	@docker build \
	--build-arg NIKOLA_VERSION=$(NIKOLA_VERSION) \
	--build-arg NIKOLA_USER=$(NIKOLA_USER) \
	-t registry.gitlab.com/padawanphysicist/nikola .

test:
	@echo "Testing image..."
	@docker run --rm \
	--volume $$PWD/assets/scripts:/home/$(NIKOLA_USER)/site \
	--workdir /home/$(NIKOLA_USER)/site \
	registry.gitlab.com/padawanphysicist/nikola \
	bash /home/$(NIKOLA_USER)/site/test.sh
	@rm --force --recursive $$PWD/assets/scripts/test/

version:
	@docker run --rm \
	registry.gitlab.com/padawanphysicist/nikola

